package stevens.software.todoapplication.mainActivity

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import stevens.software.todoapplication.database.ToDoDao
import stevens.software.todoapplication.database.ToDoDatabase
import stevens.software.todoapplication.model.ToDoInfo
import stevens.software.todoapplication.repository.ToDoRepository
import kotlinx.coroutines.*

class MainActivityViewModel(application: Application) : AndroidViewModel(application) {

    private val viewModelJob = SupervisorJob()
    private val toDoDao : ToDoDao = ToDoDatabase.getDatabase(application, viewModelScope).todoDao()
    private val repository = ToDoRepository(toDoDao)
    lateinit var todo : LiveData<ToDoInfo>
    private lateinit var allToDos : LiveData<List<ToDoInfo>>


    fun getAllToDos() : LiveData<List<ToDoInfo>> {
        loadAllToDos()
        return allToDos
    }

    private fun loadAllToDos(){
        allToDos = toDoDao.getAllToDos()
    }

    fun delete(toDo: ToDoInfo){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                repository.deleteToDo(toDo)
            }
        }
    }

    fun update(toDo: ToDoInfo) {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                repository.updateToDo(toDo)
            }
        }
    }

    fun addToDoScope(toDo: ToDoInfo) {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                repository.addTodo(toDo)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}