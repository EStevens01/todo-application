package stevens.software.todoapplication.gestureListener;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J(\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000bH\u0016\u00a8\u0006\r"}, d2 = {"Lstevens/software/todoapplication/gestureListener/GestureListener;", "Landroid/view/GestureDetector$SimpleOnGestureListener;", "()V", "onDown", "", "event", "Landroid/view/MotionEvent;", "onFling", "event1", "event2", "velocityX", "", "velocityY", "todoApp_release"})
public final class GestureListener extends android.view.GestureDetector.SimpleOnGestureListener {
    
    @java.lang.Override()
    public boolean onDown(@org.jetbrains.annotations.NotNull()
    android.view.MotionEvent event) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onFling(@org.jetbrains.annotations.NotNull()
    android.view.MotionEvent event1, @org.jetbrains.annotations.NotNull()
    android.view.MotionEvent event2, float velocityX, float velocityY) {
        return false;
    }
    
    public GestureListener() {
        super();
    }
}