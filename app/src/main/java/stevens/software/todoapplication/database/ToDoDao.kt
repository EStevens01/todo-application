package stevens.software.todoapplication.database

import androidx.lifecycle.LiveData
import androidx.room.*
import stevens.software.todoapplication.model.ToDoInfo

@Dao
interface ToDoDao {

    @Insert
     fun addTodo(todo: ToDoInfo)

    @Query("SELECT * FROM todo_table")
     fun getAllToDos(): LiveData<List<ToDoInfo>>

    @Query("SELECT * FROM todo_table WHERE todo LIKE :name")
      fun getToDoByName(name: String): LiveData<ToDoInfo>

    @Query("SELECT * FROM todo_table WHERE id LIKE :id")
     fun getToDoById(id: Int): LiveData<ToDoInfo>

    @Delete
     fun deleteToDo(todo: ToDoInfo)

    @Update
     fun updateTodo(todo: ToDoInfo)

    @Query( "UPDATE todo_table SET checked = :checked WHERE id = :id")
    fun updateTodoChecked(id: Int, checked : Int){
    }
}