package stevens.software.todoapplication.mainActivity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.todo_list_item.view.*
import stevens.software.todoapplication.R
import stevens.software.todoapplication.addToDoActivity.AddToDo
import stevens.software.todoapplication.model.ToDoInfo
import stevens.software.todoapplication.recyclerViewAdapter.ToDoRecyclerViewAdapter


class MainActivity : AppCompatActivity() {

    private lateinit var mainActivityViewModel: MainActivityViewModel

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            val receivedToDo = intent.getSerializableExtra("checkedToDo") as ToDoInfo
            mainActivityViewModel.update(receivedToDo)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,  IntentFilter("UPDATE_TOD0"));

        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        val toDoRecyclerViewAdapter =
            ToDoRecyclerViewAdapter(
                this,
                mainActivityViewModel.getAllToDos()
            )
        toDoList.layoutManager = LinearLayoutManager(this)
        toDoList.adapter = toDoRecyclerViewAdapter

        fab.setOnClickListener {
            val activityIntent = Intent(this, AddToDo::class.java)
            startActivity(activityIntent)
        }

        mainActivityViewModel.getAllToDos().observe(this, androidx.lifecycle.Observer { toDos ->
            toDos?.let {
                toDoRecyclerViewAdapter.setToDos(toDos as MutableList<ToDoInfo>)
            }
        })

        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN, ItemTouchHelper.LEFT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: ViewHolder, target: ViewHolder): Boolean {
                toDoRecyclerViewAdapter.moveToDo(viewHolder.adapterPosition, target.adapterPosition)
                return true
            }

            override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
                val toDoPosition = viewHolder.adapterPosition
                val removedToDo = toDoRecyclerViewAdapter.getToDoAtPosition(toDoPosition)

                toDoRecyclerViewAdapter.removeToDoAtPosition(toDoPosition)
                mainActivityViewModel.delete(removedToDo)
                toDoRecyclerViewAdapter.notifyItemChanged(viewHolder.adapterPosition);
                val removedToDoName = removedToDo.name

                Snackbar.make(viewHolder.itemView, "$removedToDoName deleted.", Snackbar.LENGTH_LONG).setAction(
                    R.string.snackBar_undo_text
                ){
                    toDoRecyclerViewAdapter.undoToDoDelete(removedToDo)
                    mainActivityViewModel.addToDoScope(removedToDo)
                    toDoRecyclerViewAdapter.notifyItemChanged(viewHolder.adapterPosition);
                }.show()
            }

            override fun onSelectedChanged(viewHolder: ViewHolder?, actionState: Int) {
                viewHolder?.itemView?.todoCard?.setCardBackgroundColor(ContextCompat.getColor(applicationContext,
                    R.color.selectedCardView))
                super.onSelectedChanged(viewHolder, actionState)
            }


             override fun clearView(recyclerView: RecyclerView, viewHolder: ViewHolder) {
                super.clearView(recyclerView, viewHolder)
                 viewHolder.itemView.todoCard.setCardBackgroundColor(Color.WHITE)
            }

            override fun onChildDraw(canvas: Canvas, recyclerView: RecyclerView, viewHolder: ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                val backgroundColour = "#ef5350"

                val background = ColorDrawable()
                val itemView = viewHolder.itemView

                val deleteIcon = ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_delete_white_30dp)
                val intrinsicWidth = deleteIcon?.intrinsicWidth
                val intrinsicHeight = deleteIcon?.intrinsicHeight

                val itemHeight = itemView.bottom - itemView.top

                background.color = Color.parseColor(backgroundColour)
                background.setBounds(
                    itemView.right + dX.toInt(),
                    itemView.top + 15,
                    itemView.right ,
                    itemView.bottom -15
                )
                background.draw(canvas)

                val deleteIconTop = itemView.top + (itemHeight - intrinsicHeight!!) / 2
                val deleteIconMargin = (itemHeight - intrinsicHeight) / 2
                val deleteIconLeft = itemView.right - deleteIconMargin - intrinsicWidth!!
                val deleteIconRight = itemView.right - deleteIconMargin
                val deleteIconBottom = deleteIconTop + intrinsicHeight


                deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom)
                deleteIcon.draw(canvas)

                super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

            }
        }).attachToRecyclerView(toDoList)
    }
}



