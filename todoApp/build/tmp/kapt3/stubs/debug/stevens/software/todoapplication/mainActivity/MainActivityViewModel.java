package stevens.software.todoapplication.mainActivity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\bJ\u000e\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\bJ\u0012\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006J\b\u0010\u0019\u001a\u00020\u0015H\u0002J\b\u0010\u001a\u001a\u00020\u0015H\u0014J\u000e\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\bR\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\r\u001a\b\u0012\u0004\u0012\u00020\b0\u0006X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"}, d2 = {"Lstevens/software/todoapplication/mainActivity/MainActivityViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "allToDos", "Landroidx/lifecycle/LiveData;", "", "Lstevens/software/todoapplication/model/ToDoInfo;", "repository", "Lstevens/software/todoapplication/repository/ToDoRepository;", "toDoDao", "Lstevens/software/todoapplication/database/ToDoDao;", "todo", "getTodo", "()Landroidx/lifecycle/LiveData;", "setTodo", "(Landroidx/lifecycle/LiveData;)V", "viewModelJob", "Lkotlinx/coroutines/CompletableJob;", "addToDoScope", "", "toDo", "delete", "getAllToDos", "loadAllToDos", "onCleared", "update", "todoApp_debug"})
public final class MainActivityViewModel extends androidx.lifecycle.AndroidViewModel {
    private final kotlinx.coroutines.CompletableJob viewModelJob = null;
    private final stevens.software.todoapplication.database.ToDoDao toDoDao = null;
    private final stevens.software.todoapplication.repository.ToDoRepository repository = null;
    @org.jetbrains.annotations.NotNull()
    public androidx.lifecycle.LiveData<stevens.software.todoapplication.model.ToDoInfo> todo;
    private androidx.lifecycle.LiveData<java.util.List<stevens.software.todoapplication.model.ToDoInfo>> allToDos;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<stevens.software.todoapplication.model.ToDoInfo> getTodo() {
        return null;
    }
    
    public final void setTodo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<stevens.software.todoapplication.model.ToDoInfo> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<stevens.software.todoapplication.model.ToDoInfo>> getAllToDos() {
        return null;
    }
    
    private final void loadAllToDos() {
    }
    
    public final void delete(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.model.ToDoInfo toDo) {
    }
    
    public final void update(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.model.ToDoInfo toDo) {
    }
    
    public final void addToDoScope(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.model.ToDoInfo toDo) {
    }
    
    @java.lang.Override()
    protected void onCleared() {
    }
    
    public MainActivityViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}