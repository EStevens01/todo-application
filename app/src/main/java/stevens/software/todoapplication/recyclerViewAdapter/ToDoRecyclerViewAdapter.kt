package stevens.software.todoapplication.recyclerViewAdapter

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Paint
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.core.content.res.ResourcesCompat.getColor
import androidx.lifecycle.LiveData
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.color.MaterialColors.getColor
import kotlinx.android.synthetic.main.todo_list_item.view.*
import stevens.software.todoapplication.R
import stevens.software.todoapplication.addToDoActivity.AddToDo
import stevens.software.todoapplication.model.ToDoInfo
import java.util.*

class ToDoRecyclerViewAdapter(private var context: Context, private val toDos: LiveData<List<ToDoInfo>>) :
    RecyclerView.Adapter<ToDoRecyclerViewAdapter.ToDoViewHolder>() {

    private val layoutInflater = LayoutInflater.from(context)
    private var toDoList: MutableList<ToDoInfo> = mutableListOf()
    private lateinit var deletedToDo : ToDoInfo
    private var recentlyDeletedToDoPosition : Int = -1
    private var toDoIndex : Int = -1

    override fun getItemCount() = toDoList.size

    fun setToDos(toDos: MutableList<ToDoInfo>) {
        toDoList = toDos.asReversed()
        notifyItemInserted(toDoList.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToDoViewHolder {
        val todoItemView = layoutInflater.inflate(R.layout.todo_list_item, parent, false)
        return ToDoViewHolder(todoItemView)
    }

    override fun onBindViewHolder(holder: ToDoViewHolder, position: Int) {
        val toDo = toDoList[position]

        holder.layoutToDoTextField.text = toDo.name
        holder.toDoPosition = position


        if(toDo.checked == 1) {
            holder.checkbox.isChecked = true
            setCheckedStyle(holder.layoutToDoTextField, holder.cardView)
        }
    }

    inner class ToDoViewHolder(todoItemView: View) : RecyclerView.ViewHolder(todoItemView) {
        val layoutToDoTextField: TextView = todoItemView.findViewById(R.id.toDo)
        var toDoPosition : Int = 0
        var checkbox: CheckBox = todoItemView.findViewById(R.id.checkBox)
        var cardView: CardView = todoItemView.findViewById(R.id.todoCard)


        init {
            todoItemView.setOnClickListener {
                val activityIntent = Intent(context, AddToDo::class.java)
                activityIntent.putExtra("TODO_NAME", layoutToDoTextField.text)
                context.startActivity(activityIntent)
            }

            checkbox.setOnClickListener {
                val toDo = getToDoByName(layoutToDoTextField.text.toString())

                if(checkbox.isChecked){
                    if (toDo != null) {
                        toDo.checked = 1
                        val intent: Intent = Intent("UPDATE_TOD0").putExtra("checkedToDo", toDo)
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                    }
                    setCheckedStyle(layoutToDoTextField, cardView)
                }else{
                    if (toDo != null) {
                        toDo.checked = 0
                        val intent: Intent = Intent("UPDATE_TOD0").putExtra("checkedToDo", toDo)
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                    }
                    setUncheckedStyle(layoutToDoTextField, cardView)
                }
            }
        }
    }




    fun setCheckedStyle(toDoItemView: TextView, cardView: CardView){
        toDoItemView.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        toDoItemView.setTypeface(null, Typeface.ITALIC)
        cardView.elevation = 1F
    }

    fun setUncheckedStyle(toDoItemView: TextView, cardView: CardView){
        toDoItemView.paintFlags = 0
        toDoItemView.setTypeface(null, Typeface.NORMAL)
        cardView.elevation = 11F
    }

    fun removeToDoAtPosition(position: Int) {
        deletedToDo = toDoList[position]
        recentlyDeletedToDoPosition = position
        toDoIndex = toDoList.indexOf(deletedToDo)

        toDoList.remove(deletedToDo)
        notifyItemRemoved(position);
    }

    fun undoToDoDelete(todo: ToDoInfo) {
        toDoList.add(toDoIndex, todo);
        notifyItemInserted(toDoIndex);
    }


    fun getToDoAtPosition(position: Int) : ToDoInfo {
        return toDoList[position]
    }

    fun getToDoByName(name: String) : ToDoInfo? {
        var toDo : ToDoInfo? = null
        for(element in toDoList){
            if(element.name == name) {
                toDo = element
            }
        }
        return toDo
    }

    fun moveToDo(oldPosition: Int, newPosition: Int) {
        Collections.swap(toDoList, oldPosition, newPosition)
        notifyItemMoved(oldPosition, newPosition)
    }
}









