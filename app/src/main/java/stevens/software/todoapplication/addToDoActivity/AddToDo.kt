package stevens.software.todoapplication.addToDoActivity

import android.content.Intent
import android.os.Bundle
import android.view.GestureDetector
import android.view.MotionEvent
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import stevens.software.todoapplication.gestureListener.GestureListener
import stevens.software.todoapplication.mainActivity.MainActivity
import stevens.software.todoapplication.model.ToDoInfo
import kotlinx.android.synthetic.main.content_add_todo.*
import stevens.software.todoapplication.R
import kotlin.math.abs


class AddToDo : AppCompatActivity() {

    private lateinit var  addToDoViewModel: AddToDoViewModel
    private  var toDoName : String? = null
    private lateinit var gestureDetector: GestureDetector

    private var downXCoordinate = 0f
    private var upXCoordinate = 0f
    private val minDistance = 150


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_add_todo)
        gestureDetector = GestureDetector(this,
            GestureListener()
        )

        var toDo: ToDoInfo? = null
        addToDoViewModel = ViewModelProviders.of(this).get(AddToDoViewModel::class.java)
        toDoName = intent.getStringExtra("TODO_NAME")

        if (toDoName != null) {
            addToDoViewModel.getToDoByName(toDoName!!).observe(this, Observer { todo ->
                todo?.let {
                    toDo = todo
                    displayClickedOnToDo(todo)
                }
            })
        }

        saveButton.setOnClickListener {
            if (toDo == null) {
                saveToDatabase()
            } else {
                updateDatabase(toDo!!)
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        gestureDetector.onTouchEvent(event)

        when (event.action) {
            MotionEvent.ACTION_DOWN -> downXCoordinate = event.x
            MotionEvent.ACTION_UP -> {
                upXCoordinate = event.x
                val deltaX = upXCoordinate - downXCoordinate

                if (abs(deltaX) > minDistance) {
                    if (upXCoordinate > downXCoordinate) {
                        finish()
                    }
                }
            }
        }
        return super.onTouchEvent(event)
    }

    private fun updateDatabase(toDo : ToDoInfo){
        val enteredToDo = addTodoText.text.toString()
        toDo.name = enteredToDo

        if(checkIfToDoEmpty(enteredToDo)){
            addToDoViewModel.update(toDo)
            startMainActivity()
        }
    }

    private fun showAlert(){
        val alertDialog: AlertDialog? = this.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle(R.string.alert_toDo_cannot_be_empty)
            builder.apply {
                setPositiveButton(R.string.alert_Ok) { dialog, id ->
                    dialog.dismiss()
                }
            }
            builder.create()
        }
        alertDialog?.show()
    }


    private fun displayClickedOnToDo(toDo: ToDoInfo){
        addTodoText.setText(toDo.name)
    }

    private fun saveToDatabase(){
        val enteredToDo = addTodoText.text.toString()

        if(checkIfToDoEmpty(enteredToDo)){
            addToDoViewModel.addToDo(
                ToDoInfo(
                    name = enteredToDo
                )
            )
            startMainActivity()
        }
    }

    private fun checkIfToDoEmpty(toDoName : String) : Boolean{
        return if(toDoName.isEmpty()){
            showAlert()
            false
        }else{
            true
        }
    }

     private fun startMainActivity(){
        val activityIntent = Intent(this, MainActivity::class.java)
        startActivity(activityIntent)
    }
}

