package stevens.software.todoapplication

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.*
import androidx.room.Room
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import stevens.software.todoapplication.database.ToDoDao
import stevens.software.todoapplication.database.ToDoDatabase
import stevens.software.todoapplication.model.ToDoInfo
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class TestToDoDao {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var toDoDatabase : ToDoDatabase
    private lateinit var toDoDao: ToDoDao
    private val toDoTestName = "some toDo"

    @Before
    fun initialiseDatabase() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        toDoDatabase = Room.inMemoryDatabaseBuilder(appContext, ToDoDatabase::class.java).allowMainThreadQueries().build()
        toDoDao = toDoDatabase.todoDao()
    }


    @Test
    fun testGetAllToDos_empty() {
        val empty = ArrayList<ToDoInfo>()

        toDoDao.getAllToDos().observeOnce {
           assertEquals(it, empty)

        }
    }

    @Test
    fun testAddToDo(){
        val toDo =
            ToDoInfo(1, toDoTestName, 0)
        val toDoList = ArrayList<ToDoInfo>()
        toDoList.add(toDo)

        toDoDao.addTodo(toDo)

        toDoDao.getAllToDos().observeOnce {
            assertEquals(it, toDoList)

        }
    }

    @Test
    fun testDeleteTodo(){
        val toDo =
            ToDoInfo(1, toDoTestName, 0)
        val emptyList = ArrayList<ToDoInfo>()

        toDoDao.addTodo(toDo)
        toDoDao.deleteToDo(toDo)

        toDoDao.getAllToDos().observeOnce {
            assertEquals(it, emptyList)

        }
    }

    @Test
    fun testUpdateToDo(){
        val orignalToDo =
            ToDoInfo(1, toDoTestName, 0)

        val toDo =
            ToDoInfo(
                1,
                "some updated todo",
                0
            )
        val toDoList = ArrayList<ToDoInfo>()
        toDoList.add(toDo)

        toDoDao.addTodo(orignalToDo)
        toDoDao.updateTodo(toDo)

        toDoDao.getAllToDos().observeOnce {
            assertEquals(it, toDoList)

        }
    }

    private fun <T> LiveData<T>.observeOnce(onChangeHandler: (T) -> Unit) {
        val observer =
            OneTimeObserver(handler = onChangeHandler)
        observe(observer, observer)
    }

}

class OneTimeObserver<T>(private val handler: (T) -> Unit) : Observer<T>, LifecycleOwner {
    private val lifecycle = LifecycleRegistry(this)

    init {
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    override fun getLifecycle(): Lifecycle = lifecycle

    override fun onChanged(t: T) {
        handler(t)
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    }
}