package stevens.software.todoapplication.repository

import androidx.lifecycle.LiveData
import stevens.software.todoapplication.model.ToDoInfo
import stevens.software.todoapplication.database.ToDoDao

class ToDoRepository(private val toDoDao: ToDoDao) {

    val allToDos: LiveData<List<ToDoInfo>> = toDoDao.getAllToDos()

    suspend fun addTodo(todo: ToDoInfo) {
        toDoDao.addTodo(todo)
    }

    suspend fun deleteToDo(todo: ToDoInfo){
        toDoDao.deleteToDo(todo)
    }

    suspend fun updateToDo(todo: ToDoInfo){
        toDoDao.updateTodo(todo)
    }

}

