package stevens.software.todoapplication.addToDoActivity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0012\u0010\u0014\u001a\u00020\u00112\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0014J\u0010\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\b\u0010\u001a\u001a\u00020\u0011H\u0002J\b\u0010\u001b\u001a\u00020\u0011H\u0002J\b\u0010\u001c\u001a\u00020\u0011H\u0002J\u0010\u0010\u001d\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lstevens/software/todoapplication/addToDoActivity/AddToDo;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "addToDoViewModel", "Lstevens/software/todoapplication/addToDoActivity/AddToDoViewModel;", "downXCoordinate", "", "gestureDetector", "Landroid/view/GestureDetector;", "minDistance", "", "toDoName", "", "upXCoordinate", "checkIfToDoEmpty", "", "displayClickedOnToDo", "", "toDo", "Lstevens/software/todoapplication/model/ToDoInfo;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onTouchEvent", "event", "Landroid/view/MotionEvent;", "saveToDatabase", "showAlert", "startMainActivity", "updateDatabase", "todoApp_debug"})
public final class AddToDo extends androidx.appcompat.app.AppCompatActivity {
    private stevens.software.todoapplication.addToDoActivity.AddToDoViewModel addToDoViewModel;
    private java.lang.String toDoName;
    private android.view.GestureDetector gestureDetector;
    private float downXCoordinate = 0.0F;
    private float upXCoordinate = 0.0F;
    private final int minDistance = 150;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onTouchEvent(@org.jetbrains.annotations.NotNull()
    android.view.MotionEvent event) {
        return false;
    }
    
    private final void updateDatabase(stevens.software.todoapplication.model.ToDoInfo toDo) {
    }
    
    private final void showAlert() {
    }
    
    private final void displayClickedOnToDo(stevens.software.todoapplication.model.ToDoInfo toDo) {
    }
    
    private final void saveToDatabase() {
    }
    
    private final boolean checkIfToDoEmpty(java.lang.String toDoName) {
        return false;
    }
    
    private final void startMainActivity() {
    }
    
    public AddToDo() {
        super();
    }
}