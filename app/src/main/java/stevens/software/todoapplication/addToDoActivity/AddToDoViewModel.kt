package stevens.software.todoapplication.addToDoActivity

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import stevens.software.todoapplication.database.ToDoDao
import stevens.software.todoapplication.database.ToDoDatabase
import stevens.software.todoapplication.model.ToDoInfo
import stevens.software.todoapplication.repository.ToDoRepository
import kotlinx.coroutines.*

class AddToDoViewModel(application: Application) : AndroidViewModel(application){

    private val viewModelJob = SupervisorJob()
    private val toDoDao : ToDoDao = ToDoDatabase.getDatabase(application, viewModelScope).todoDao()
    private val repository =
        ToDoRepository(toDoDao)
    lateinit var todo : LiveData<ToDoInfo>

    fun addToDo(toDo: ToDoInfo){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                repository.addTodo(toDo)
            }
        }
    }

    fun getToDoByName(name: String) : LiveData<ToDoInfo> {
        todo = toDoDao.getToDoByName(name)
        return todo
    }

    fun update(toDo: ToDoInfo) {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                repository.updateToDo(toDo)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}


