package stevens.software.todoapplication

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import stevens.software.todoapplication.mainActivity.MainActivity
import stevens.software.todoapplication.model.ToDoInfo
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(AndroidJUnit4ClassRunner::class)
class TestMainActivity {

    private val toDoName = "test todo"
    private val snackBarText = "test todo deleted."
    private val updatedToDoName = "updated test todo"

    @Rule
    @JvmField
    var mainActivity: ActivityTestRule<MainActivity> =
        IntentsTestRule(MainActivity::class.java, true, false)

    private fun launchActivityWithIntent() {
        val intent = Intent()
        mainActivity.launchActivity(intent)
    }

    @Test
    fun test_1_AddNewToDo_Empty() {
        launchActivityWithIntent()
        onView(withId(R.id.fab)).perform(click())
        onView(withId(R.id.addTodoText)).perform(typeText(""))
        onView(withId(R.id.saveButton)).perform(click())
        onView(withText("ToDo cannot be empty")).check(matches(isDisplayed()))
    }

    @Test
    fun test_2_AddToDo() {
        launchActivityWithIntent()

        addToDoHelper()

        onView(withId(R.id.saveButton)).check(matches(isDisplayed()))
        onView(withId(R.id.saveButton)).perform(click())
    }


    @Test
    fun test_3_checkingAndUnCheckingToDo() {
        launchActivityWithIntent()

        //Add to-do and save it
        addToDoHelper()
        onView(withId(R.id.saveButton)).perform(click())

        onView(withId(R.id.checkBox)).check(matches(isDisplayed()))
        onView(withId(R.id.checkBox)).perform(click()).check(matches(isChecked()))

        onView(withId(R.id.checkBox)).perform(click()).check(matches(isNotChecked()))
    }

    @Test
    fun test_4_updateToDo() {
        launchActivityWithIntent()

        //Add to-do and save it
        addToDoHelper()
        onView(withId(R.id.saveButton)).perform(click())

        onView(withId(R.id.toDo)).perform(click())
        onView(withId(R.id.addTodoText)).check(matches(withText(toDoName)))

        onView(withId(R.id.addTodoText)).perform(replaceText(updatedToDoName))
        onView(withId(R.id.saveButton)).perform(click())

        onView(withId(R.id.toDo)).check(matches(withText(updatedToDoName)))
    }

    @Test
    fun test_5_deleteToDo_UndoDelete() {
        launchActivityWithIntent()

        //Add to-do and check it is being displayed in the main activity
        addToDoHelper()
        onView(withId(R.id.saveButton)).perform(click())
        onView(withId(R.id.toDo)).check(matches(isDisplayed()))
        onView(withId(R.id.toDo)).check(matches(withText(toDoName)))

        //Swipe to delete to-do
        onView(withId(R.id.toDoList)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, swipeLeft()))

        //Check snackBar is being displayed and undo the delete
        onView(withId(com.google.android.material.R.id.snackbar_text)).check(matches(isDisplayed()))
        onView(withId(com.google.android.material.R.id.snackbar_text)).check(matches(withText(snackBarText)))

        onView(withId(com.google.android.material.R.id.snackbar_action)).perform(click())

        //check undo has rendered the to-do
        onView(withId(R.id.toDo)).check(matches(isDisplayed()))
        onView(withId(R.id.toDo)).check(matches(withText(toDoName)))

        onView(withId(R.id.toDoList)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, swipeLeft()))

    }

    private fun addToDoHelper() {
        val toDo = ToDoInfo(1, toDoName, 0)
        onView(withId(R.id.fab)).perform(click())
        onView(withId(R.id.addTodoText)).perform(typeText(toDo.name))
    }
}