package stevens.software.todoapplication.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000eJ\u0019\u0010\u000f\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000eJ\u0019\u0010\u0010\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000eR\u001d\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0011"}, d2 = {"Lstevens/software/todoapplication/repository/ToDoRepository;", "", "toDoDao", "Lstevens/software/todoapplication/database/ToDoDao;", "(Lstevens/software/todoapplication/database/ToDoDao;)V", "allToDos", "Landroidx/lifecycle/LiveData;", "", "Lstevens/software/todoapplication/model/ToDoInfo;", "getAllToDos", "()Landroidx/lifecycle/LiveData;", "addTodo", "", "todo", "(Lstevens/software/todoapplication/model/ToDoInfo;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "deleteToDo", "updateToDo", "todoApp_debug"})
public final class ToDoRepository {
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.List<stevens.software.todoapplication.model.ToDoInfo>> allToDos = null;
    private final stevens.software.todoapplication.database.ToDoDao toDoDao = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<stevens.software.todoapplication.model.ToDoInfo>> getAllToDos() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object addTodo(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.model.ToDoInfo todo, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object deleteToDo(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.model.ToDoInfo todo, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object updateToDo(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.model.ToDoInfo todo, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    public ToDoRepository(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.database.ToDoDao toDoDao) {
        super();
    }
}