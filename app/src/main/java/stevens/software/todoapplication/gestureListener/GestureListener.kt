package stevens.software.todoapplication.gestureListener

import android.view.GestureDetector
import android.view.MotionEvent

class GestureListener : GestureDetector.SimpleOnGestureListener() {

    override fun onDown(event: MotionEvent): Boolean {
        return true
    }

    override fun onFling(event1: MotionEvent, event2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
        return true
    }
}
