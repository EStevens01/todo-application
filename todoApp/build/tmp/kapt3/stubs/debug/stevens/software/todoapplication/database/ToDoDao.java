package stevens.software.todoapplication.database;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0014\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\t0\bH\'J\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010\u000b\u001a\u00020\fH\'J\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\b2\u0006\u0010\u000e\u001a\u00020\u000fH\'J\u0010\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0018\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0012\u001a\u00020\fH\u0017\u00a8\u0006\u0013"}, d2 = {"Lstevens/software/todoapplication/database/ToDoDao;", "", "addTodo", "", "todo", "Lstevens/software/todoapplication/model/ToDoInfo;", "deleteToDo", "getAllToDos", "Landroidx/lifecycle/LiveData;", "", "getToDoById", "id", "", "getToDoByName", "name", "", "updateTodo", "updateTodoChecked", "checked", "todoApp_debug"})
public abstract interface ToDoDao {
    
    @androidx.room.Insert()
    public abstract void addTodo(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.model.ToDoInfo todo);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM todo_table")
    public abstract androidx.lifecycle.LiveData<java.util.List<stevens.software.todoapplication.model.ToDoInfo>> getAllToDos();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM todo_table WHERE todo LIKE :name")
    public abstract androidx.lifecycle.LiveData<stevens.software.todoapplication.model.ToDoInfo> getToDoByName(@org.jetbrains.annotations.NotNull()
    java.lang.String name);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM todo_table WHERE id LIKE :id")
    public abstract androidx.lifecycle.LiveData<stevens.software.todoapplication.model.ToDoInfo> getToDoById(int id);
    
    @androidx.room.Delete()
    public abstract void deleteToDo(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.model.ToDoInfo todo);
    
    @androidx.room.Update()
    public abstract void updateTodo(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.model.ToDoInfo todo);
    
    @androidx.room.Query(value = "UPDATE todo_table SET checked = :checked WHERE id = :id")
    public abstract void updateTodoChecked(int id, int checked);
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
        
        @androidx.room.Query(value = "UPDATE todo_table SET checked = :checked WHERE id = :id")
        public static void updateTodoChecked(stevens.software.todoapplication.database.ToDoDao $this, int id, int checked) {
        }
    }
}