package stevens.software.todoapplication.recyclerViewAdapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001*B!\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0012\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006\u00a2\u0006\u0002\u0010\tJ\b\u0010\u0013\u001a\u00020\u000fH\u0016J\u000e\u0010\u0014\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\u000fJ\u0010\u0010\u0016\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0017\u001a\u00020\u0018J\u001c\u0010\u0019\u001a\u00020\u001a2\n\u0010\u001b\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0015\u001a\u00020\u000fH\u0016J\u001c\u0010\u001c\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u000fH\u0016J\u000e\u0010 \u001a\u00020\u001a2\u0006\u0010\u0015\u001a\u00020\u000fJ\u0016\u0010!\u001a\u00020\u001a2\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%J\u0014\u0010&\u001a\u00020\u001a2\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\b0\u0012J\u0016\u0010\'\u001a\u00020\u001a2\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%J\u000e\u0010(\u001a\u00020\u001a2\u0006\u0010)\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n \r*\u0004\u0018\u00010\f0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\b0\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"}, d2 = {"Lstevens/software/todoapplication/recyclerViewAdapter/ToDoRecyclerViewAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lstevens/software/todoapplication/recyclerViewAdapter/ToDoRecyclerViewAdapter$ToDoViewHolder;", "context", "Landroid/content/Context;", "toDos", "Landroidx/lifecycle/LiveData;", "", "Lstevens/software/todoapplication/model/ToDoInfo;", "(Landroid/content/Context;Landroidx/lifecycle/LiveData;)V", "deletedToDo", "layoutInflater", "Landroid/view/LayoutInflater;", "kotlin.jvm.PlatformType", "recentlyDeletedToDoPosition", "", "toDoIndex", "toDoList", "", "getItemCount", "getToDoAtPosition", "position", "getToDoByName", "name", "", "onBindViewHolder", "", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "removeToDoAtPosition", "setCheckedStyle", "toDoItemView", "Landroid/widget/TextView;", "cardView", "Landroidx/cardview/widget/CardView;", "setToDos", "setUncheckedStyle", "undoToDoDelete", "todo", "ToDoViewHolder", "todoApp_debug"})
public final class ToDoRecyclerViewAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<stevens.software.todoapplication.recyclerViewAdapter.ToDoRecyclerViewAdapter.ToDoViewHolder> {
    private final android.view.LayoutInflater layoutInflater = null;
    private java.util.List<stevens.software.todoapplication.model.ToDoInfo> toDoList;
    private stevens.software.todoapplication.model.ToDoInfo deletedToDo;
    private int recentlyDeletedToDoPosition = -1;
    private int toDoIndex = -1;
    private android.content.Context context;
    private final androidx.lifecycle.LiveData<java.util.List<stevens.software.todoapplication.model.ToDoInfo>> toDos = null;
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void setToDos(@org.jetbrains.annotations.NotNull()
    java.util.List<stevens.software.todoapplication.model.ToDoInfo> toDos) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public stevens.software.todoapplication.recyclerViewAdapter.ToDoRecyclerViewAdapter.ToDoViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.recyclerViewAdapter.ToDoRecyclerViewAdapter.ToDoViewHolder holder, int position) {
    }
    
    public final void setCheckedStyle(@org.jetbrains.annotations.NotNull()
    android.widget.TextView toDoItemView, @org.jetbrains.annotations.NotNull()
    androidx.cardview.widget.CardView cardView) {
    }
    
    public final void setUncheckedStyle(@org.jetbrains.annotations.NotNull()
    android.widget.TextView toDoItemView, @org.jetbrains.annotations.NotNull()
    androidx.cardview.widget.CardView cardView) {
    }
    
    public final void removeToDoAtPosition(int position) {
    }
    
    public final void undoToDoDelete(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.model.ToDoInfo todo) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final stevens.software.todoapplication.model.ToDoInfo getToDoAtPosition(int position) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final stevens.software.todoapplication.model.ToDoInfo getToDoByName(@org.jetbrains.annotations.NotNull()
    java.lang.String name) {
        return null;
    }
    
    public ToDoRecyclerViewAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.List<stevens.software.todoapplication.model.ToDoInfo>> toDos) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0016X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a\u00a8\u0006\u001b"}, d2 = {"Lstevens/software/todoapplication/recyclerViewAdapter/ToDoRecyclerViewAdapter$ToDoViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "todoItemView", "Landroid/view/View;", "(Lstevens/software/todoapplication/recyclerViewAdapter/ToDoRecyclerViewAdapter;Landroid/view/View;)V", "cardView", "Landroidx/cardview/widget/CardView;", "getCardView", "()Landroidx/cardview/widget/CardView;", "setCardView", "(Landroidx/cardview/widget/CardView;)V", "checkbox", "Landroid/widget/CheckBox;", "getCheckbox", "()Landroid/widget/CheckBox;", "setCheckbox", "(Landroid/widget/CheckBox;)V", "layoutToDoTextField", "Landroid/widget/TextView;", "getLayoutToDoTextField", "()Landroid/widget/TextView;", "toDoPosition", "", "getToDoPosition", "()I", "setToDoPosition", "(I)V", "todoApp_debug"})
    public final class ToDoViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView layoutToDoTextField = null;
        private int toDoPosition = 0;
        @org.jetbrains.annotations.NotNull()
        private android.widget.CheckBox checkbox;
        @org.jetbrains.annotations.NotNull()
        private androidx.cardview.widget.CardView cardView;
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getLayoutToDoTextField() {
            return null;
        }
        
        public final int getToDoPosition() {
            return 0;
        }
        
        public final void setToDoPosition(int p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.CheckBox getCheckbox() {
            return null;
        }
        
        public final void setCheckbox(@org.jetbrains.annotations.NotNull()
        android.widget.CheckBox p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.cardview.widget.CardView getCardView() {
            return null;
        }
        
        public final void setCardView(@org.jetbrains.annotations.NotNull()
        androidx.cardview.widget.CardView p0) {
        }
        
        public ToDoViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View todoItemView) {
            super(null);
        }
    }
}