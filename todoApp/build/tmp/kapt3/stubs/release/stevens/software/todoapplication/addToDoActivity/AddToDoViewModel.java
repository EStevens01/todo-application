package stevens.software.todoapplication.addToDoActivity;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u000bJ\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u0016\u001a\u00020\u0017J\b\u0010\u0018\u001a\u00020\u0013H\u0014J\u000e\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u000bR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lstevens/software/todoapplication/addToDoActivity/AddToDoViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "repository", "Lstevens/software/todoapplication/repository/ToDoRepository;", "toDoDao", "Lstevens/software/todoapplication/database/ToDoDao;", "todo", "Landroidx/lifecycle/LiveData;", "Lstevens/software/todoapplication/model/ToDoInfo;", "getTodo", "()Landroidx/lifecycle/LiveData;", "setTodo", "(Landroidx/lifecycle/LiveData;)V", "viewModelJob", "Lkotlinx/coroutines/CompletableJob;", "addToDo", "", "toDo", "getToDoByName", "name", "", "onCleared", "update", "todoApp_release"})
public final class AddToDoViewModel extends androidx.lifecycle.AndroidViewModel {
    private final kotlinx.coroutines.CompletableJob viewModelJob = null;
    private final stevens.software.todoapplication.database.ToDoDao toDoDao = null;
    private final stevens.software.todoapplication.repository.ToDoRepository repository = null;
    @org.jetbrains.annotations.NotNull()
    public androidx.lifecycle.LiveData<stevens.software.todoapplication.model.ToDoInfo> todo;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<stevens.software.todoapplication.model.ToDoInfo> getTodo() {
        return null;
    }
    
    public final void setTodo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<stevens.software.todoapplication.model.ToDoInfo> p0) {
    }
    
    public final void addToDo(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.model.ToDoInfo toDo) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<stevens.software.todoapplication.model.ToDoInfo> getToDoByName(@org.jetbrains.annotations.NotNull()
    java.lang.String name) {
        return null;
    }
    
    public final void update(@org.jetbrains.annotations.NotNull()
    stevens.software.todoapplication.model.ToDoInfo toDo) {
    }
    
    @java.lang.Override()
    protected void onCleared() {
    }
    
    public AddToDoViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}